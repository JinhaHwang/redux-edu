# Redux 교육
## Redux란?
![](readme/DFA24670-2603-45F5-8F86-C5B603169567.png)
> 자바스크립트 앱을 위한  
> 예상 가능한 상태 컨테이너  

## 특징 또는 장점
![](readme/7C4EBF85-79EB-4469-9181-C2F824D2D4FA.png)
* Predictable
* Centralized
* Debuggable
* Flexible

## Redux 맛보기
**순서**
1. 노드 프로젝트 생성
2. redux 패키지 설치
3. 맛보기

::준비::
```bash
// 프로젝트 디렉터리 생성
$ mkdir redux-edu

$ cd redux-edu

// 노드 프로젝트 만들기
$ npm init -y

// 깃 버전 관리 시작하기
$ git init

// .girignore 파일 만들기
$ curl https://www.gitignore.io/api/macos%2Ceclipse%2Cwebstorm%2Cintellij%2Cvisualstudiocode%2Cnode -o .gitignore

// redux-starter-kit 노드 패키지 설치하기
$ npm i --save 또는 -S redux

// 초기환경 커밋
$ git add . && git commit -m "initial commit"
```

::store 생성 샘플 코드 작성::
**edu1-store.js**
``` javascript
import { createStore } from 'redux'

function counter(state = 0, action) {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1
        case 'DECREMENT':
            return state - 1
        default:
            return state
    }
}

let store = createStore(counter)

/*
    스토어를 구독하기 위해 함수 등록
    dispatch시 아래 subscribe에 전달하는 함수가 실행된다.
 */
store.subscribe(() => console.log(store.getState()))

store.dispatch({ type: 'INCREMENT' })
// 1
store.dispatch({ type: 'INCREMENT' })
// 2
store.dispatch({ type: 'DECREMENT' })
// 1
```

::샘플 실행해보기::
```bash
$ node edu1-store
```

**하지만 에러가 발생**

::es6 문법의 자바스크립트 실행을 위해 babel-node 설치::
```bash
// es6 문법의 자바스크립트 실행을 위한 babel-node
$ npm install --save-dev 또는 -D @babel/core @babel/node
```
> node 실행환경은 기본적으로 CommonJS 기반의 문법이다.  
> 브라우저는 ECMA Script를 따른다.  
> 모던브라우저의 경우 es6+ 지원, es6, 7, 8…  

::로컬에 설치된 babel-node를 실행::
```bash
$ npx babel-node edu1-store.js 또는 edu1-store
```

**하지만 또 에러 발생**
해당코드를 어떤 환경으로 transpile하여 실행할지 preset을 지정해줘야 한다.

::@babel/preset-env 패키지 설치 후 다시 실행::
```bash
// 패키지 설치
$ npm i -D @babel/preset-env

// presets 옵션을 추가하여 스크립트 실행
$ npx babel-node --presets @babel/env ./edu1-store
```

::npm 스크립트 추가해보기::
**package.json**
```json
"scripts": {
    "babel-run-with": "npx babel-node --presets @babel/env"
  }
```

::npm 스크리트를 이용하여 실행::
```bash
// 실행을 위한 필수 옵션에만 집중할 수 있다.
$ npm run babel-run-with ./eud1-store
```

::.babelrc.js 설정파일 작성하기::
npm 프로젝트 루트에 .babelrc 파일 또는 .babelrc.js 모듈을 작성하면
babel 트랜스파일시 .babelrc에 작성된 환경을 따른다.
**.babelrc.js**
```javascript
// @babel/preset-env 패키지가 있기 때문에 아래 옵션을 설정가능
module.exports = {
    presets: [
        ['@babel/env', {
            targets: {
                node: 'current',
            },
        }],
    ],
}
```

::맛보기::
```bash
$ npx babel-node ./edu1-store
```


## 설치한 패키지 및 설정파일 정리
**package.json**
```json
{
  "name": "redux-edu",
  "version": "1.0.0",
  "description": "redux education",
  "repository": "localhost:8888",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "babel-run-with": "npx babel-node --presets @babel/env",
    "start:dev": "webpack-dev-server"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "immutable": "^4.0.0-rc.12",
    "react": "^16.8.6",
    "react-dom": "^16.8.6",
    "react-redux": "^6.0.1",
    "redux": "^4.0.1"
  },
  "devDependencies": {
    "@babel/core": "^7.4.3",
    "@babel/node": "^7.2.2",
    "@babel/plugin-proposal-class-properties": "^7.4.0",
    "@babel/plugin-proposal-object-rest-spread": "^7.4.3",
    "@babel/plugin-syntax-dynamic-import": "^7.2.0",
    "@babel/plugin-transform-async-to-generator": "^7.4.0",
    "@babel/preset-env": "^7.4.3",
    "@babel/preset-react": "^7.0.0",
    "babel-eslint": "^10.0.1",
    "babel-loader": "^8.0.5",
    "eslint": "^5.16.0",
    "eslint-plugin-react": "^7.12.4",
    "html-webpack-plugin": "^3.2.0",
    "prop-types": "^15.7.2",
    "react-dev-utils": "^8.0.0",
    "react-hot-loader": "^4.8.2",
    "uglifyjs-webpack-plugin": "^2.1.2",
    "webpack": "^4.29.6",
    "webpack-cli": "^3.3.0",
    "webpack-dev-server": "^3.2.1"
  }
}
```

**.gitignore**
```bash
// 터미널 또는 git bash에서
$ curl https://www.gitignore.io/api/macos%2Ceclipse%2Cwebstorm%2Cintellij%2Cvisualstudiocode%2Cnode -o .gitignore
```

**.babelrc.js**
```javascript
module.exports = {
    presets: [
        [
            '@babel/react',
            {
                '@babel/env': {
                    module: false,
                },
            },
        ],
    ],
    plugins: [
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-transform-async-to-generator',
        '@babel/plugin-syntax-dynamic-import',
        'react-hot-loader/babel',
    ],
}
```

**.eslintrc.js**
```javascript
module.exports = {
    plugins: ['react'],
    extends: ['eslint:recommended', 'plugin:react/recommended'],
    parser: 'babel-eslint',
    rules: {
        'no-console': 'off',
        // 'no-undef': 'off',
        // 'no-unused-vars': 'off',
        'react/prop-types': 'off',
    },
    env: {
        browser: true,
        node: true,
    },
}
```

**.prettierrc.js**
```javascript
// prettier.config.js or .prettierrc.js
module.exports = {
    tabWidth: 4,
    useTabs: false,
    printWidth: 80,
    trailingComma: "all",
    semi: false,
    singleQuote: true,
    jsxSingleQuote: false,
    arrowParens: 'always',
    bracketSpacing: true,
    jsxBracketSameLine: false,
};
```

**webpack.config.js**
```javascript
const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ignoredFiles = require('react-dev-utils/ignoredFiles')
const eslintFormatter = require('react-dev-utils/eslintFormatter')
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin')


// const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

module.exports = {
    module: {
        rules: [
            {
                test: /\.(js|jsx|mjs)$/,
                enforce: 'pre',
                use: [
                    {
                        options: {
                            formatter: eslintFormatter,
                            eslintPath: require.resolve('eslint'),
                            quiet: true,
                        },
                        loader: require.resolve('eslint-loader'),
                    },
                ],
                include: path.resolve('src'),
            },
            {
                test: /\.js$/,
                include: [path.resolve(__dirname, 'src')],
                loader: 'babel-loader',
                options: {
                    extends: path.resolve(__dirname, '.babelrc'),
                    sourceMap: true,
                    cacheDirectory: true,
                },
            },
        ],
    },

    entry: [
        require.resolve('react-dev-utils/webpackHotDevClient'),
        require.resolve('@babel/polyfill'),
        path.resolve(__dirname, 'src/index.js'),
    ],
    output: {
        chunkFilename: '[name].[chunkhash].js',
        filename: '[name].[hash].js',
    },

    mode: 'development',

    plugins: [
        new HtmlWebpackPlugin({
            inject: true,
            template: path.resolve(__dirname, 'public/index.html'),
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new WatchMissingNodeModulesPlugin(path.resolve('node_modules')),
    ],

    devServer: {
        hot: true,
        inline: true,
        host: '0.0.0.0',
        port: 8888,
        contentBase: path.resolve('public'),
        watchContentBase: true,
        watchOptions: {
            ignored: ignoredFiles(path.resolve('src')),
        },
        disableHostCheck: true,
        historyApiFallback: true,
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendors: {
                    priority: -10,
                    test: /[\\/]node_modules[\\/]/,
                },
            },

            chunks: 'async',
            minChunks: 1,
            minSize: 30000,
            name: true,
        },
    },
}
```


## 공식사이트 Links
[Redux 공식사이트](https://redux.js.org)
[babel 공식사이트](https://babeljs.io)
[eslint 공식사이트](https://eslint.org)
[prettier 공식사이트](https://prettier.io/)

## 블로그 Links
[babel7 설명 잘 된 블로그](https://blog.cometkim.kr/posts/start-modern-javascript-with-babel/)
