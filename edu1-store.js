import { createStore } from 'redux'

// 순수 함수로 작성
function counter(state = 0, action) {
    switch (action.type) {
        case 'INCREMENT':
            return state + 1
        case 'DECREMENT':
            return state - 1
        default:
            return state
    }
}

let store = createStore(counter)

/*
    스토어를 구독하기 위해 함수 등록
    dispatch시 아래 subscribe에 전달하는 함수가 실행된다.
 */
store.subscribe(() => console.log(store.getState()))

store.dispatch({ type: 'INCREMENT' })
// 1
store.dispatch({ type: 'INCREMENT' })
// 2
store.dispatch({ type: 'DECREMENT' })
// 1
